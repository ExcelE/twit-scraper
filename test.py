from __future__ import print_function
import json, csv, re
from pprint import pprint
from os.path import join, dirname
from watson_developer_cloud import ToneAnalyzerV3
from watson_developer_cloud.tone_analyzer_v3 import ToneInput
from tweet_parser.tweet import Tweet
from tweet_parser.tweet_parser_errors import NotATweetError
import fileinput

anger = joy = fear = sadness = analytical = confident = tentative = 0

emotionList = ['anger', 'joy', 'fear', 'sadness', 'analytical', 'confident', 'tentative']
emotionCount = [0,0,0,0,0,0,0]

tone_analyzer = ToneAnalyzerV3(
    iam_api_key='6B6idckJqW-YH2EBIiveQhgQVh_rYgbe9fxzwe59d8Ge',
    version='2017-09-21',
    url="https://gateway-wdc.watsonplatform.net/tone-analyzer/api")

# grab and store all json objects and store into a virtual one

text = 'Team, I know that times are tough! Product sales have been disappointing for the past three quarters. We have a competitive product, but we need to do a better job of selling it!'
content_type = 'application/json'

tone = tone_analyzer.tone({"text": text},content_type)

print(json.dumps(tone, indent=2))

# result = []
# allTweet = ""

# tweetOpener = open('tweets.csv')
# tweetReader = csv.reader(tweetOpener)
# for row in tweetReader:
    
#     allTweet.join(str(row))
#     # utterances.append({"text": re.sub(r" ?\([^)]+\)", "", str(row))})
#     # result.append(tone_analyzer.tone_chat(utterances))
#     # result.append(json.dumps(tone_analyzer.tone_chat(utterances)))

# utterances = []
# utterances.append({"text": str(allTweet)})
# print(json.dumps(tone_analyzer.tone_chat(utterances), indent=2))


# for utterance in utterances:
# result = [json.dumps(tone_analyzer.tone_chat(utterances))]
# print(json.dumps(tone_analyzer.tone_chat(utterances), indent=2))

