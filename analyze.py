from __future__ import print_function
import json, csv, re, multiprocessing, string, os, random
from pprint import pprint
from os.path import join, dirname
from watson_developer_cloud import ToneAnalyzerV3
from watson_developer_cloud.tone_analyzer_v3 import ToneInput
from tweet_parser.tweet import Tweet
from tweet_parser.tweet_parser_errors import NotATweetError
import fileinput, emoji, sys, traceback
from cucco import Cucco

from multiprocessing import Process

directory = 'tweets'

def batch_grabber(filename, uid):
    with open(filename, encoding='utf-8') as myfile:
        # print("Now in {}\n".format(fn))
        reader = csv.reader(myfile)
        data = [r for r in reader]

        length = 0
        utterances = ""

        for tweet in data:
            if (length % 90 == 0 and length > 0):
                file_writer(utterances, length, uid)
                utterances = ""
            cleanedTweet = textCleaner(tweet[2])
            utterances += cleanedTweet
            length += 1
        else: # if eof and the length of utterances < 90
            file_writer(utterances, length, uid)

def runAnalyzer():
    # grab and store all json objects and store into a virtual one
    raw_count = 0
    raw_list_names = [] # stores all the file locations to be offloaded to multiprocessing
    raw_uid = [] # marks some unique id
    for fn in os.listdir(directory): # Start reading all the json files in the folder
        curr_file = os.path.join(directory, fn)
        if(os.path.getsize(curr_file) > 0):
            with open(curr_file, encoding='utf-8') as myfile:
                raw_count += sum(1 for row in myfile)
            raw_list_names.append(curr_file) # Appends "tweets/tweets-(2018-6-20).csv"
            first, sep1, last = fn.partition("(") # Ex: "tweets-(2018-6-20).csv" => "tweets-", "(", "2018-6-20).csv"
            head, sep2, end = last.partition(")") # Ex: "2018-6-20)" => "2018-6-20", ")", ".csv)"
            raw_uid.append(head) # Appends "2018-6-20"

    input_tuple = zip(raw_list_names, raw_uid)

    try:
        with multiprocessing.Pool(processes=multiprocessing.cpu_count()-1) as pool:
            pool.starmap(batch_grabber, input_tuple)
    except Exception as e:
        traceback.print_exc()
        raise e
    finally:
        print("Ending Sentiment Analysis...")
    return raw_count
        

def textCleaner(text): # Returns cleaned text. 
    # Remove emojis
    cucco = Cucco()
    final = cucco.replace_emojis(text)
    # Link removals
    removedLinks0 = re.sub(r"http\S+", "", final) # link is http
    removedLinks1 = re.sub(r"pic.twitter\S+", "", removedLinks0) # link is pic.twitter
    # final = re.sub(r'[?|$|!|@]',r'',removedLinks2)
    filter_escape = re.compile(r'\x1b[^m]*m') # removes ascii/escape sequences
    removedLinks2 = filter_escape.sub('', removedLinks1)
    return removedLinks2
    # ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)|", " ", removedLinks2).split())


def file_writer(text, count, uid):
    tone_analyzer = ToneAnalyzerV3(
        iam_api_key='6B6idckJqW-YH2EBIiveQhgQVh_rYgbe9fxzwe59d8Ge',
        version='2017-09-21',
        url="https://gateway-wdc.watsonplatform.net/tone-analyzer/api")

    content_type = 'application/json'

    tweet = textCleaner(text)

    with open("tweets-analyzed/tone-analyzed-{}-({}).json".format(count, uid), 'w') as writer:
        tone = tone_analyzer.tone({'text': tweet}, content_type, sentences=True)
        # print(json.dumps(tone, indent=2))
        json.dump(tone, writer, indent=4)

if __name__ == '__main__':
    runAnalyzer()