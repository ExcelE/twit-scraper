import os, json

def aws_analyzed(folder_location):
    entities = {}
    analyzed_folder = "aws-analyzed"
    file_num = 0

    for fn in os.listdir(folder_location): # Start reading all the files in the folder
        with open(os.path.join(folder_location, fn), 'r', encoding='utf-8') as myfile:
            text = json.load(myfile)
            for entity in text['Entities']:
                if (entity['Text'] in entities):
                    entities[entity['Text']] += 1
                else:
                    entities[entity['Text']] = 1

    for entity in entities:
        print("{}, {}".format(entity, entities[entity]))

if __name__ == '__main__':
    aws_analyzed("aws-results")