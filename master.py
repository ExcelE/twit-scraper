#!/usr/bin/python3
import twint, sys, datetime, multiprocessing, shutil, os, time
from multiprocessing import Process
from itertools import product
from pathlib import Path

tweet_folder = 'tweets'

def startSearch(hashtag, time_begin, time_end, output, location):

    path_to_out = "tweets/tweets-({}).csv".format(time_begin)
    Path(path_to_out).touch()

    c = twint.Config()
    # c.Count = True
    # c.Stats = True
    # c.Format = "{username}"
    c.Output = path_to_out
    c.Search = hashtag
    c.Near = location
    c.Store_csv = True
    c.Limit = 200
    c.Lang = "en"
    c.Custom = ['date', 'time','tweet']
    c.Since = time_begin
    c.Until = time_end
    # RUN
    twint.run.Search(c)

def time_splitter(hashtag, delta, location):
    begin_date = datetime.date.today()
    days = int(delta)

    start_dates = []
    end_dates = []
    out_labels = []
    
    anchor_date = begin_date
    for x in range(days):
        d1 = anchor_date - datetime.timedelta(days=x+1)
        start_dates.append(str(d1))
        d2 = anchor_date - datetime.timedelta(days=x)
        end_dates.append(str(d2))
        out_labels.append(x)

    hashtags = [hashtag] * days
    locations = [location] * days
    input_tuple = zip(hashtags, start_dates, end_dates, out_labels, locations)
    with multiprocessing.Pool(processes=multiprocessing.cpu_count()-1) as pool:
        pool.starmap(startSearch, input_tuple)

    return [end_dates[days-1], end_dates[0]]

def check_location(file_loc):
    if not os.path.exists(file_loc):
        os.makedirs(file_loc)

def clean_location(tweet_folder):
    # remove all analyzed tweets
    try:
        shutil.rmtree(tweet_folder)
    except Exception as e:
        print(e)

def init_scrape(topic, delta, location):
    clean_location(tweet_folder)
    check_location(tweet_folder)
    time.sleep(1)
    return time_splitter(topic, delta, location)


if __name__ == '__main__':
    argv = sys.argv

    if len(argv) == 4:
        init_scrape(argv[1], argv[2], argv[3])
    else:
        raise Exception("Wrong number of arguments.")

