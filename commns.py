from master import *
from analyze import *
from report import *
from datetime import datetime
import os, shutil, time

tweet_dir = {"step-1":"tweets","step-2":"tweets-analyzed","step-3a":"emotion-buckets", "step-3b": "aws-results", "reports":"Tweet-Analysis.txt"}
switcher = True
total_time = step1_time = step2_time = step3_time = 0.0
tweet_location = ""
tweet_time_range = []
raw_tweet_count = 0

def time_output(location, elapsed):
    return "Time elapsed at {}: {:.2f} sec".format(location, elapsed)

def filename_with_date():
    FORMAT = '%Y-%m-%d-(%H:%M)'
    path = 'Topic-Analysis.txt'
    new_path = '%s_%s' % (datetime.now().strftime(FORMAT), path)
    return new_path

def path_creator(path_name):
    try:
        if not os.path.exists(path_name):
            os.makedirs(path_name)
    except Exception as e:
        print(e)

def step1(hashtag, delta, location): # Scrape the tweets
    start = time.time()
    global tweet_time_range
    tweet_time_range = init_scrape(hashtag, delta, location)
    global step1_time, tweet_location
    tweet_location = location
    step1_time = time.time() - start

def step2(): # Run Tone analysis for each
    path_creator(tweet_dir['step-2'])
    start = time.time()
    global raw_tweet_count
    raw_tweet_count = runAnalyzer()
    global step2_time, total_time
    step2_time = time.time() - start

def step3(hashtag): # Construct a report
    
    json_output = {}
    file_location = "Tweet-Analysis.txt"
    only_emotions = "emotions-analyzed.json"
    start = time.time()
    global raw_tweet_count
    with open(file_location, 'w') as writer:
        writer.write("Topic: {}\nRaw tweets pulled: {}\nGeneral location of tweets: {}\nStart date: {}\nEnd date: {}\n\n"\
            .format(hashtag, raw_tweet_count, tweet_location, tweet_time_range[0], tweet_time_range[1]))
    json_output = report(tweet_dir['step-2'], file_location)
    global step1_time 
    step3_time = time.time() - start

    with open(file_location, "a") as writer:
        writer.write("\n{}\n{}\n{}".format(time_output("step1",step1_time),time_output("step2",step2_time),time_output("step3",step3_time)))
    
    total = 0
    with open("total.temp", "r", encoding="utf-8") as writer:
        total = writer.read()
    os.remove("total.temp")

    json_output["Summary"] = {}
    json_output["Summary"]["Runtime Analysis"] = {}

    json_output["Summary"]["Topic"] = hashtag
    json_output["Summary"]["Start"] = tweet_time_range[0]
    json_output["Summary"]["End"] = tweet_time_range[1]
    json_output["Summary"]["Location"] = tweet_location
    json_output["Summary"]["Raw Tweets"] = raw_tweet_count
    json_output["Summary"]["Processed Tweets"] = int(total)

    json_output["Summary"]["Runtime Analysis"]["Step1"] = float("{0:.2f}".format(step1_time))
    json_output["Summary"]["Runtime Analysis"]["Step2"] = float("{0:.2f}".format(step2_time))
    json_output["Summary"]["Runtime Analysis"]["Step3"] = float("{0:.2f}".format(step3_time))

    with open(only_emotions, "w", encoding="utf-8") as writer:
        json.dump(json_output, writer, indent=2)


def step4(): # Clean up and remove files
    print("Cleaning up files...")
    # remove all analyzed tweets
    try:
        for k,v in tweet_dir.items():
            shutil.rmtree(v)
    except Exception as e:
        print(e)

    # remove tweets.csv
    if(switcher):
        try:
            os.remove(tweet_dir['reports'])
        except OSError:
            print()
        print('Removed!')

if __name__ == '__main__':
    argv = sys.argv
    if(len(argv) == 4):
        step4()
        switcher = False
        step1(argv[1], argv[2], argv[3])
        step2()
        step3(argv[1])
        # step4()
    else: 
        print("Wrong number of arguments! Try again!")
