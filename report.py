import os, json, objectpath, csv, boto3, shutil
import requests
import sys
import urllib
from collections import OrderedDict
from pymongo import MongoClient
from pathlib import Path

emotion_buckets = "emotion-buckets"

def send_to_Comprehend(folder_loc):
    to_send = []
    aws_folder = "aws-results"
    comprehend = boto3.client("comprehend")
    check_location("{}".format(aws_folder))

    file_num = 0
    for fn in os.listdir(folder_loc): # Start reading all the  files in the folder
        with open(os.path.join(folder_loc, fn), "r", encoding="utf-8") as myfile:
            for line in myfile:
                if (sys.getsizeof(to_send) + sys.getsizeof(line) < 500): # if current buffer + string < 500 bytes, append
                    to_send.append(line)
                elif (sys.getsizeof(to_send) > 500): # Check if its not empty, send to amazon
                    emotion, sep, tail = fn.partition(".") # Separate the filename emotion.txt > emotion
                    text = " ".join(to_send)
                    with open("{}/aws-result-{}-{}.json".format(aws_folder, emotion, file_num), "w", encoding="utf-8") as writer:
                        entities = comprehend.detect_entities(Text=text, LanguageCode="en")
                        json.dump(entities, writer, indent=2)
                    file_num += 1
                else: 
                    pass
            else:
                emotion, sep, tail = fn.partition(".") # Separate the filename emotion.txt > emotion
                to_send.append(line)
                text = " ".join(to_send)
                with open("{}/aws-result-({})-{}.json".format(aws_folder, emotion, file_num), "w", encoding="utf-8") as writer:
                    entities = comprehend.detect_entities(Text=text, LanguageCode="en")
                    json.dump(entities, writer, indent=2)
        to_send = []

def uid_extraction(current):
    first, mid, last = current.partition("(") # Ex: "tweets-(2018-6-20).csv" => "tweets-", "(", "2018-6-20).csv"
    head, mid, end = last.partition(")") # Ex: "2018-6-20)" => "2018-6-20", ")", ".csv)"
    return head

def clear_location():
    # remove all analyzed tweets
    try:
        shutil.rmtree(emotion_buckets)
    except Exception as e:
        print(e)
    # # remove tweets.csv
    # try:
    #     os.remove("tweets.csv")
    # except OSError:
    #     print()

def check_location(file_loc):
    if not os.path.exists(file_loc):
        os.makedirs(file_loc)

def report(directory, output_file):
    labeled_emotions = ["joy","anger","fear","sadness","confident","analytical","tentative"]
    emotions = [0] * 7
    clear_location()
    check_location(emotion_buckets)
    check_location("data")

    json_output = {}
    json_output["Daily"] = {}

    for fn in os.listdir(directory): # Start reading all the json files in the folder
        curr_filename = uid_extraction(fn)
        temp_emotions = {"joy": 0, "anger": 0, "sadness": 0, "confident": 0, "analytical": 0, "tentative": 0}
        with open(os.path.join(directory, fn), "r", encoding="utf-8") as myfile:
            text = json.load(myfile)
            try:
                raw_data = text["sentences_tone"]
                for dt in raw_data:
                    for emotion_list in dt["tones"]:
                        emotion = emotion_list["tone_id"]
                        emotions[labeled_emotions.index("{}".format(emotion))] += 1
                        temp_emotions[emotion] += 1
                        with open("{}/{}.txt".format(emotion_buckets, emotion), "a", encoding="utf-8") as writer:
                            writer.write("{}\n".format(dt["text"]))
                if curr_filename in json_output["Daily"]:
                    old_data = json_output["Daily"][curr_filename]
                    for emotion in old_data:
                        old_data[emotion] += temp_emotions[emotion]
                else:
                    json_output["Daily"][curr_filename] = temp_emotions
            except:
                pass

    allEmotions = 0
    send_to_Comprehend(emotion_buckets)
    
    for digit in emotions:
        allEmotions += digit

    with open("total.temp", "w") as writer:
        writer.write(str(allEmotions))

    curr = 0
    with open(output_file, "a") as writer:
        for num in emotions:
            # writer.write("For {}: ".format(labeled_emotions[curr]))
            writer.write("For {}: {} out of {}: {:.0%} of the tweets\n".format(labeled_emotions[curr], num, allEmotions, num/allEmotions))
            curr += 1

    aws_analyzed("aws-results", output_file)

    # with open(only_emotions, "w", encoding='utf-8') as writer:
    #     for emotion in labeled_emotions:
    return json_output


def aws_analyzed(folder_location, output_file):
    entities = {}
    current_emotion = ""

    for fn in os.listdir(folder_location): # Start reading all the files in the folder
        first, sep, last = fn.partition("(") # format: "aws-result-(analytical)-0.json" => "aws-result-", "(", "analytical)-0"
        emotion, sep, end = last.partition(")") # format: "analytical", ")", "-0"
        current_emotion = emotion
        with open(os.path.join(folder_location, fn), "r", encoding="utf-8") as myfile:
            # print(fn)
            text = json.load(myfile)
            for entity in text["Entities"]:
                # print(entity["Text"])
                if (entity["Text"] in entities):
                    entities[entity["Text"]] += 1
                else:
                    entities[entity["Text"]] = 1
        tempDict = OrderedDict(sorted(entities.items(), key=lambda t: t[1], reverse=True))
        with open(output_file, "a") as writer:
            writer.write("\n{}: (Showing top 10)\n".format(current_emotion))
            curr = 1
            for entity in tempDict:
                writer.write("\t'{}' was mentioned {}\n".format(entity, tempDict[entity]))
                curr += 1
                if (curr > 10):
                    break
        entities = {}





if __name__ == "__main__":
    report("tweets-analyzed", "Tweet-Analysis.txt")
    # argv = sys.argv

    # if len(argv) == 3:
    #     report(argv[1], argv[2])
    # else:
    #     raise Exception("Wrong number of arguments.")
