# Twit-Scraper (temp)

This is a script that analyzes tweets filtered based on components such as date, topic and geographic location the tweets were posted. We add a bit of NLP to deliver a big data approach for analyzing a certain topic on differing locations.

## Getting Started

1) Clone this repo to your local environment.
2) Determine where to install dependencies:
    2a) Global 
    2b) Virtual environment
3) Install dependencies:
```
    pip3 install -r requirements.txt
```
4) Start analysis:
```
    python3 ./commns.py [topic] [number of days] [general location]
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## To Do

* [Feature] Multi-location tracking and analysis
* [Production] Deploy on a cloud instance
* [Production] A recurring script to scan and analyze on some interval
* [Production] Database for handling search queries for arbitrary data points

## Changelog (v0.2.0)
* ADDED: exporting daily data points to a JSON file

